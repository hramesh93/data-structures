#include <iostream>
#include <vector>

using namespace std;

// Sorting an elements in an array using bubble sort algorithm
vector<int> bubbleSort( vector<int> vec ) {
	int n = vec.size();
	for( int i = 0; i < n-1; i++ ) {
		for( int j = 0; j < n - 2 - i; j++) {
			if( vec[j] > vec[j+1] ) {
				int temp = vec[j];
				vec[j] = vec[j+1];
				vec[j+1] = temp;
			}
		}
	}
	return vec;
}

int main() {
	
	// Creating an unordered array
	vector<int> arr;
	arr.push_back(23);
	arr.push_back(78);
	arr.push_back(45);
	arr.push_back(8);
	arr.push_back(32);
	arr.push_back(56);
	
	// Sorting the unordered array using bubble sort technique
	arr = bubbleSort(arr);
	
	// Printing all the elements of the sorted array
	for( int i = 0; i < arr.size(); i++ ) {
		cout << arr[i] << " ";
	}
	return 0;
}
